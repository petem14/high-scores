const jsonBody = require("body/json");
var scores = [{ name: "Edwin", score: 50 }, { name: "David", score: 39 }];
let resources = {
    "/scores": JSON.stringify(scores)
};

const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    if (req.method === "GET") {
        if (resources[req.url] === undefined) {
            res.statusCode = 404;
            res.end("ERROR NOT FOUND");
        } else {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/javascript');
            const responseBody = resources[req.url];
            res.end(responseBody);
        }
    } else if (req.method === "POST") {
        res.statusCode = 201;
        res.setHeader('Content-Type', 'application/javascript');
        jsonBody(req, res, function (err, body){
            scores.push(body)
            scores.sort(function(a,b){
                return b.score - a.score
            })
            scores.splice(3)
            resources["/scores"] = JSON.stringify(scores)
            res.end()
        })
    }
    // console.log(req.url);
    // console.log(req.headers);
    // console.log(req.method);
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});